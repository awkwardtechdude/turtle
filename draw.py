import turtle
t = turtle.Pen()
COLORS = ['red', 'yellow', 'green', 'blue', 'orange', 'purple', 'pink', 'cyan', 'magenta']
sides = 6
turtle.bgcolor("black")
t.speed(0)
sides = int(turtle.textinput('Enter the number of the shape\'s sides', 'How many sides to do you want?'))
offset = float(turtle.textinput('Offset degrees', 'By how many degrees do you want to turn the shape?'))
for x in range(1000):
    t.pencolor(COLORS[x%sides])
    #t.penup()
    t.forward(x*4)
    #t.pendown()
    t.left(360 / sides  + float(offset))
    t.width(4)
